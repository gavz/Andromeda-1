# Andromeda

Andromeda makes initial reverse engineering work of android applications bit faster and easier.
Compared to other alternatives, it's written in C/C++ and has a noticeable performance advantage.

## DEMO
[![demo](https://user-images.githubusercontent.com/16405698/65391224-5a716500-dd6f-11e9-9de3-b3dcbc5e27ad.png)](https://www.youtube.com/watch?v=doeg-tCX-sg)


## Author
Lasha Khasaia ([@_qaz_qaz](https://twitter.com/_qaz_qaz))

## Note
I've very limited experience with Android, so if we have time please contribute to add new features.